<?php 

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    include('config/db_connect.php');

    //write query for all pizzas
    $sql = 'SELECT pizzaName, ingredients, id FROM pizzas ORDER BY createdAt';

    //make query and get result
    $result = mysqli_query($conn, $sql);

    //fetch the resulting rows as an array
    $pizzas = mysqli_fetch_all($result, MYSQLI_ASSOC);

    //free results from memory 
    mysqli_free_result($result);

    //close connection
    mysqli_close($conn);

    // print_r($pizzas);
?>

<!DOCTYPE html>
<html lang="en">

<?php include('templates/header.php'); ?>

<h4 class="center black-text">Pizza Receipts Library</h4>

<div class="container">
    <div class='row'>

        <?php foreach($pizzas as $pizza) :?>
                
            <div class="col s6 m3">
                <div class="card z-depth-0">
                    <img 
                    src="https://freesvg.org/img/1502140004.png"
                    class="pizza"
                    >
                    <div class="card-content center">
                        <h6> <?php echo htmlspecialchars($pizza['pizzaName']) ?> </h6>

                        <ul>
                            <?php 
                            $ingrList = explode(",", $pizza['ingredients']);
                            foreach($ingrList as $ingr) : ?>
                                <li><?php echo htmlspecialchars($ingr) ?></li>
                            <?php endforeach; ?>
                        </ul>

                    </div>

                    <div class="card-action right-align">
                        <a href="details.php?id= <?php echo $pizza['id'] ?>" class="brand-text">more info</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

        <!-- <?php if(count($pizzas) >= 3) : ?>
        <p>There are more than 3 pizzas</p>
        <?php else: ?>
        <p> There are less than 3 pizzas</p>
        <?php endif; ?> -->

    </div>
</div>

<?php include('templates/footer.php');?>


</html>